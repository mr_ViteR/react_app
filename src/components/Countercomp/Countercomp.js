import React from 'react';
import styles from './Countercomp.css'
export default ({value, increment, decrement, requestPokemonList}) =>  (
  <div className={styles.normal}>
    value: {value} <br />

    <button onClick={requestPokemonList}>requestPokemonList</button>
    <button onClick={increment}> increment </button>
    <button onClick={decrement}>decrement</button>
  </div>

);
