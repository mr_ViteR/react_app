import {connect} from 'react-redux';
import Countercomp from './Countercomp.js';
import {requestPokemonList, increment, decrement} from '../../Action/Action';


const mapStateToProps = (state) => {
  console.log(state);
  return {
    value: state.counter
  }
}

function mapDispatchToProps(dispatch) {
  return {
    requestPokemonList: () => dispatch(requestPokemonList),
    increment: () => dispatch(increment()),
    decrement: () => dispatch(decrement())


  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Countercomp);
