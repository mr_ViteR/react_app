import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import {createStore, applyMiddleware } from 'redux';
import reducer from './reducer'
const middlewares = [thunk];

if (process.env.NODE_ENV === `development`) {
  const logger = createLogger();
  middlewares.push(logger);
}
console.log(process.env.NODE_ENV)

export default createStore(reducer, applyMiddleware(
  ...middlewares
))
