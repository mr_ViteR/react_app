import request from '../utils/request';

export function  increment () {
       return {
         type: 'INCREMENT'}
     }
export function decrement () {
       return {
         type: 'DECREMENT'}
     }
export function recievePokemonList(args) {
console.log(args);
return {
  type: 'GET_POKEMONS'
  }
}
 export function requestPokemonList() {
   return (dispatch) => {
     dispatch({type: 'FETCHING_POKEMONS'});
     request('http://pokeapi.co/api/v2/pokemon')
     .then((response) => dispatch(recievePokemonList(response)))
   }
 }
export function asyncAction() {
  return (dispatch) => {
    dispatch(requestPokemonList());
    setTimeout(() => dispatch(increment()), 2000 )
  }
}
