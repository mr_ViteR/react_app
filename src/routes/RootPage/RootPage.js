import React from 'react';
import styles from './RootPage.css';
export default ({children}) => {
  return (
    <div className={styles.normal}>
      {children}
    </div>
  );
}
