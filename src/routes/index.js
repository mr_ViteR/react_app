import { Router, Route, IndexRoute, browserHistory} from 'react-router';
import IndexPage from './IndexPage';
import RootPage from './RootPage';
import HomePage from './HomePage';
import React from 'react';


export default () => (
  <Router history={browserHistory}>
    <Route path="/" component={RootPage}>
      <IndexRoute component={IndexPage} />
      <Route path="home" component={HomePage} />
    </Route>
    <Route path="*" component={() => <div>404!<br /><a href="/">Reset</a></div>} />
  </Router>
);
